
#include <math.h>


//typedef double data_t;
//void RK4_LBE();
const double h = (double)(1.0) * pow(10.0, -6.0);
const double C1 = (double)10.0 * pow(10.0, -9.0);
const double C2 = (double)100.0 * pow(10.0, -9.0);
const double L = (double)19.0 * pow(10.0, -3.0);
const double R = (double)1800.00;
const double m0 = (double)-0.37 * pow(10.0, -3.0);
const double m1 = (double)-0.68 * pow(10.0, -3.0);
const double Bp = (double)1.1;
const double eq1 = (double)((double) 1.0 / (double(10.0) * pow(10.0, -9.0)));
const double eq2 = (double)((double) 1.0 / (double(100.0) * pow(10.0, -9.0)));
const double eq3 = (double)-((double) 1.0 / (double(19.0) * pow(10.0, -3.0)));

