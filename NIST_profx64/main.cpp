#include"stdio.h"
#include"Header.h"
#include"math.h"
#include <bitset>

#include"fp2bin.h"


int main() {

	// Get the real value


	char binString[23];
	double K1 = 0, K2 = 0, K3 = 0, K4 = 0, L1 = 0, L2 = 0, L3 = 0, L4 = 0, M1 = 0, M2 = 0, M3 = 0, M4 = 0;


	double x, y, z;
	x = 0.500986764831543;
	y = -0.2;
	z = 0.0;

	double x2, y2, z2;
	x2 = 0.500986764831543;
	y2 = -0.2;
	z2 = 0.0;

	double n;
	int k = 0;

	double xd = 0;
	double Xeq = 0;
	double Xequ = 0;

	double yd = 0;

	double zd = 0;

	double id;

	FILE* fp;
	fp = fopen("../../Nist/lbe64.txt", "w");


	for (int i = 0; i < 90000; i++) {

		// printf("resultado de eq1 === %f\n", x);






		K1 = 0.0;
		K2 = 0.0;
		K3 = 0.0;
		K4 = 0.0;
		L1 = 0.0;
		L2 = 0.0;
		L3 = 0.0;
		L4 = 0.0;
		M1 = 0.0;
		M2 = 0.0;
		M3 = 0.0;
		M4 = 0.0;

		xd = 0.0;
		yd = 0.0;
		zd = 0.0;


		Xeq = fabs(log10(fabs(x - x2) / 2));

		Xeq = Xeq * pow(10, 15);
		Xequ = fmod(Xeq, pow(2, 32));
			if (i > 25000 and i <= 90000) {
		fprintf(fp, "%f \n", Xequ);
		}




		if (x > Bp) {
			id = (m0 * x + Bp * (m1 - m0));
		}
		else if (x < -Bp) {
			id = (m0 * x + Bp * (m0 - m1));
		}
		else {
			id = (m1 * x);
		}


		K1 = eq1 * ((y - x) / R - id);
		L1 = eq2 * ((x - y) / R + z);
		M1 = eq3 * y;


		xd = x + h * (0.5) * K1;
		yd = y + h * (0.5) * L1;
		zd = z + (0.5) * h * M1;



		if (xd > Bp) {
			id = m0 * xd + Bp * (m1 - m0);
		}
		else if (xd < (-Bp)) {
			id = m0 * xd + Bp * (m0 - m1);
		}
		else {
			id = m1 * xd;
		}

		K2 = eq1 * ((yd - xd) / R - id);
		L2 = eq2 * ((xd - yd) / R + zd);
		M2 = eq3 * yd;


		xd = x + h * (0.5) * K2;
		yd = y + h * (0.5) * L2;
		zd = z + (0.5) * h * M2;

		if (xd > Bp) {
			id = m0 * xd + Bp * (m1 - m0);
		}
		else if (xd < (-Bp)) {
			id = m0 * xd + Bp * (m0 - m1);
		}
		else {
			id = m1 * xd;
		}
		K3 = eq1 * ((yd - xd) / R - id);
		L3 = eq2 * ((xd - yd) / R + zd);
		M3 = eq3 * yd;


		xd = x + h * K3;
		yd = y + h * L3;
		zd = z + h * M3;



		if (xd > Bp) {
			id = m0 * xd + Bp * (m1 - m0);
		}
		else if (xd < (-Bp)) {
			id = m0 * xd + Bp * (m0 - m1);
		}
		else {
			id = m1 * xd;
		}

		K4 = eq1 * ((yd - xd) / R - id);
		L4 = eq2 * ((xd - yd) / R + zd);
		M4 = eq3 * yd;;





		x = x + h * (1.0 / 6.0) * (K1 + (2.0) * K2 + (2.0) * K3 + K4);
		y = y + h * (1.0 / 6.0) * (L1 + (2.0) * L2 + (2.0) * L3 + L4);
		z = z + h * (1.0 / 6.0) * (M1 + (2.0) * M2 + (2.0) * M3 + M4);



		K1 = 0;
		K2 = 0;
		K3 = 0;
		K4 = 0;
		L1 = 0;
		L2 = 0;
		L3 = 0;
		L4 = 0;
		M1 = 0;
		M2 = 0;
		M3 = 0;
		M4 = 0;
		xd = 0;
		yd = 0;
		zd = 0;


		// exte


		if (x2 > Bp) {
			id = (m0 * x2 + Bp * (m1 - m0));
		}
		else if (x2 < -Bp) {
			id = (m0 * x2 + Bp * (m0 - m1));
		}
		else {
			id = (m1 * x2);
		}


		K1 = eq1 * (y2 / R - x2 / R - id);
		L1 = eq2 * ((x2 - y2) / R + z2);
		M1 = eq3 * y2;


		xd = x2 + h * (0.5) * K1;
		yd = y2 + h * (0.5) * L1;
		zd = z2 + (0.5) * h * M1;



		if (xd > Bp) {
			id = m0 * xd + Bp * (m1 - m0);
		}
		else if (xd < (-Bp)) {
			id = m0 * xd + Bp * (m0 - m1);
		}
		else {
			id = m1 * xd;
		}

		K2 = eq1 * (yd / R - xd / R - id);
		L2 = eq2 * ((xd - yd) / R + zd);
		M2 = eq3 * yd;

		xd = x2 + h * (0.5) * K2;
		yd = y2 + h * (0.5) * L2;
		zd = z2 + (0.5) * h * M2;

		if (xd > Bp) {
			id = m0 * xd + Bp * (m1 - m0);
		}
		else if (xd < (-Bp)) {
			id = m0 * xd + Bp * (m0 - m1);
		}
		else {
			id = m1 * xd;
		}
		K3 = eq1 * (yd / R - xd / R - id);
		L3 = eq2 * ((xd - yd) / R + zd);
		M3 = eq3 * yd;



		xd = x2 + h * K3;
		yd = y2 + h * L3;
		zd = z2 + h * M3;



		if (xd > Bp) {
			id = m0 * xd + Bp * (m1 - m0);
		}
		else if (xd < (-Bp)) {
			id = m0 * xd + Bp * (m0 - m1);
		}
		else {
			id = m1 * xd;
		}

		K4 = eq1 * (yd / R - xd / R - id);
		L4 = eq2 * ((xd - yd) / R + zd);
		M4 = eq3 * yd;





		x2 = x2 + h * (1.0 / 6.0) * (K1 + (2.0) * K2 + (2.0) * K3 + K4);
		y2 = y2 + h * (1.0 / 6.0) * (L1 + (2.0) * L2 + (2.0) * L3 + L4);
		z2 = z2 + h * (1.0 / 6.0) * (M1 + (2.0) * M2 + (2.0) * M3 + M4);




		printf("%d \n", i);
	}



	fclose(fp);
}
