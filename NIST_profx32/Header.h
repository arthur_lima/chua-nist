
#include <math.h>


//typedef float data_t;
//void RK4_LBE();
const float h = (float)(1.0) * powf(10.0, -6.0);
const float C1 = (float)10.0 * powf(10.0, -9.0);
const float C2 = (float)100.0 * powf(10.0, -9.0);
const float L = (float)19.0 * powf(10.0, -3.0);
const float R = (float)1800.00;
const float m0 = (float)-0.37 * powf(10.0, -3.0);
const float m1 = (float)-0.68 * powf(10.0, -3.0);
const float Bp = (float)1.1;
const float eq1 = (float)((float) 1.0 / (float(10.0) * powf(10.0, -9.0)));
const float eq2 = (float)((float) 1.0 / (float(100.0) * powf(10.0, -9.0)));
const float eq3 = (float)-((float) 1.0 / (float(19.0) * powf(10.0, -3.0)));

